FFT wykorzystane w tym zadaniu to szybka transformata Fouriera, sygna� w dziedzinie czasu zostaje zamieniony na
sygna� w dziedzinie cz�stotliwo�ci. Okna czasowy s�u�� wy�wietleniu zamienionego juz sygna�u.
Okna te maj� r�ne skale, oraz wzory na wy�wietlenie funkcji, co sprawia, �e wyst�puj� mi�dzy nimi r�nice 
w wy�wietlaniu sygna�u. Na przyk�ad okno Bartletta pokazuje wy�sze warto�ci kra�cowych cz�ci sygna�u ni�
okno Hanninga, �rodek sygna�u we wszystkich oknach jest mniej wi�cej podobny, r�ni� si� g��wnie
kra�cami sygna�u.

przecieki w widmie - okna czasowe

Przecieki w widmie - w zale�no�ci od dok�adno�ci pr�bkowania (fs/N) tworzony jest wykres o okreslonej dok�adno�ci. Wyst�puj� jednak sygna�y
na cz�stotliwo�ciach po�rednich (np. pr�bkowana jest cz�stotliwo�� 0,4 i 0,6), je�li sygna� wyst�puje na cz�stotliwo�ci 0,57, to zostanie dodany na 
wykresie do 0,6. Te nieci�g�o�ci sprawiaj�, �e wykres sygna�u nigdy nie b�dzie wygl�da� jak rzeczywisty sygna�. Mo�emy to ogranicza� poprzez
ustawianie coraz wi�kszej cz�stotliwo�ci, ale wtedy czas przetwarzania b�dzie du�o d�u�szy.

