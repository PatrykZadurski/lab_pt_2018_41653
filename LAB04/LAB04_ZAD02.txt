1. Przy zmianie Quantization Interval na ni�szy zwi�ksza si� wysoko�� schodk�w,
   a przy zmianie na wy�szy ich wysoko�� si� zmniejsza (czyli wykres staje si� bardziej szczeg�owy)

2. Przy zmianie Quantization Interval na ni�szy zmniejsza si� szczeg�owo��, gdy� zmniejszamy ilo��
   pr�bek pobranych w zadanym okresie, analogicznie, tylko odwrotnie w przypadku ustawienia parametru na wy�szy

3. Zwi�kszenie szczeg�owo�ci wykresu skutkuje ni�szym mse, wykresy zaczynaj� coraz bardziej si� przypomina�

Qi = 1/30
Qr = 8
MSE = 0.0073

Qi = 1/50
Qr = 20
MSE = 0.0026

Qi = 1/200
Qr = 100
MSE = 0.00016

Zmiana Qi i Qr wp�ywa na szczeg�owo�� wykresu. Przy zmianie Qi na ni�sz� a Qr na wy�sz� warto�� wyres staje sie bardziej szczeg�owy.
Ilo�� zmian sygna�u na przestrzeni 1 sekundy okre�la Qi, np. 1/8 oznacza �e sygna� b�dzie si� zmienia� co 0.125 sekundy. Powi�kszenie 
szczeg�owo�ci wykresu sprawia, �e przestaje by� tak kwadratowy, a zaczyna przypomina� sinusoid� (Qi = 1/200, Qr = 100 sprawia, �e
r�nica mi�dzy sinusoid� a wygenerowanym wykresem jest znikoma). Przy bardziej szczeg�owym wykresie zmniejsza si� b��d �redniokwadratowy,
poniewa� odleg�o�ci mi�dzy punktami na sinusoidzie, a tymi na wykresie s� mniejsze ni� w przypadku sygna�u o ni�szej szczeg�owo�ci.