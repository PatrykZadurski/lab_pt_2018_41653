function [ L ] = averageCodeLength(codeBook, sortedProbabilityDistribution )

code_length=cellfun('length',flipud(codeBook))
for k=1:length(codeBook)
length_vector(k)=code_length(k,2);

end

L=sum(sortedProbabilityDistribution.*length_vector');

end