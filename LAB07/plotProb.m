function [] = plotProb( sortedAlphabet, sortedProbabilityDistribution )
% ************************
% Plot probability
%  By Cezary Wernik
% ************************
    labels=cell(size(sortedAlphabet));
    for i=1:max(size(sortedAlphabet))
        labels{i}=[sortedAlphabet(i)];
    end
    figure;
    bar(sortedProbabilityDistribution);
    set(gca,'xticklabel',labels);
    title('Probability of alphabet');
    xlabel('Character');
    ylabel('Probability');
end
