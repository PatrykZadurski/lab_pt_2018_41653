function [sortedAlphabet,sortedProbabilityDistribution] = getProb( data, alphabet )
% ************************
% Get probability
%  By Cezary Wernik
% ************************
N=max(size(data));
sortedProbabilityDistribution=zeros(max(size(alphabet)),1);
i=1;
for c=alphabet
    sortedProbabilityDistribution(i)=length(find(data==c))/N;
    i=i+1;
end
[sortedProbabilityDistribution,idx] = sort(sortedProbabilityDistribution,'ascend');
sortedAlphabet=alphabet(idx);
end
