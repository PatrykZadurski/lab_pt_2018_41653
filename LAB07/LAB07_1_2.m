close all
clear all
clc

% Wiadomo�� do przes�ania 
 msg='rtrtrtgygygygggghuhhhhhuuhhu';

% Dopuszczalny s�ownik kodowy
alphabet=['r','t','g','y','h','u'];

% Szacowanie prawdopodobie�stw wyst�pienia znak�w ze s�ownika kodowego
[ sortedAlphabet, sortedProbabilityDistribution ] = getProb( msg, alphabet );

% Rysowanie wykresu prawdopodobie�stwa wyst�pienia znak�w ze s�ownika kodowego
plotProb( sortedAlphabet, sortedProbabilityDistribution );

% Obliczanie drzewa kodowego Huffmana
[ codeBook ] = huffTree( sortedAlphabet, sortedProbabilityDistribution )

% Kodowanie danych na podstawie obliczonego drzewa
[ encodeMsg ] = encodeHuff( codeBook, msg );

% Dekodowanie danych na podstawie obliczonego drzewa
[ msg2 ] = decodeHuff( codeBook, encodeMsg )

% Poni�ej funkcje do samodzielnej implem�acji:

% Entropia �r�d�a
 [H] = entropy( sortedProbabilityDistribution );

% �rednia d�ugo�� kodu
 [L] = averageCodeLength( codeBook, sortedProbabilityDistribution );

% Efektywno�� kodowania
 [E] = efficiency( H, L );