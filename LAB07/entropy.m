function [ H ] = entropy(sortedProbabilityDistribution );
H=sortedProbabilityDistribution.*log2(sortedProbabilityDistribution);
H=sum(H)*(-1);
end
