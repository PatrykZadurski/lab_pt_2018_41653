%wygenerowanie danych testowych (wektor pionowy)
disp('dane wej�ciowe');
data=[0 1 0 0]'

%INDEX LOSOWANIE
index=0;
  index=rand(1);
  index=index*7;
  index=round(index);

%zakodowanie kodem korekcyjnym Hamminga(7,4)
disp('dane zakodowane');
encode_data=h74_encode(data)

%%%%%%%%%%%%%%%%%%%%%
%      Funkcja      %
%%%%%%%%%%%%%%%%%%%%%

%celowe uszkodzenie 3-ego lub losowego bitu
disp('dane uszkodzone');
encode_data=spoilBit(index,encode_data)



%korekcja i odkodowanie danych
disp('dane zrekonstruowane i wskazanie kt�ry indeks by� naprawiany');
[decode_data,index]=h74_decode(encode_data)

%por�wnanie danych odkodowanych z wej�ciowymi
disp('por�wnanie danych zrekonstruowanych i wej�ciowych');
[decode_data' data]