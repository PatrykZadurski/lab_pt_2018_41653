function [decode_data,index]=h74_decode(encode_data)
H=[1 0 1 0 1 0 1; 0 1 1 0 0 1 1; 0 0 0 1 1 1 1]
p=H*encode_data';
p=mod(p,2);
index=p(1)*1+p(2)*2+p(3)*4;
if index==0
    decode_data=encode_data([3 5 6 7])
else encode_data(index)=~encode_data(index)
    decode_data=encode_data([3 5 6 7])   
end