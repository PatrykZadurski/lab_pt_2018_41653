function [ rU ] = IRLE64( uU )

	N=length(uU);
	n=1;

	I=1;
	rU(I)=int8(0);

	while 1
	   if uU(n)<0
		  % unique reading
		  for k=n+1:n+abs(uU(n))
			 rU(I)=uU(k);
			 I=I+1;
		  end
		  n=n+abs(uU(n))+1;
	   else
		  % repeatable reading
		  for k=1:uU(n)
			 rU(I)=uU(n+1);
			 I=I+1;
		  end
		  n=n+1+1;
	   end
	   if n>=N;
		  break;
	   end
	end
end