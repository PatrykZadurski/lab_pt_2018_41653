function [ rY ] = IRLE64( uY )

	N=length(uY);
	n=1;

	I=1;
	rY(I)=int8(0);

	while 1
	   if uY(n)<0
		  % unique reading
		  for k=n+1:n+abs(uY(n))
			 rY(I)=uY(k);
			 I=I+1;
		  end
		  n=n+abs(uY(n))+1;
	   else
		  % repeatable reading
		  for k=1:uY(n)
			 rY(I)=uY(n+1);
			 I=I+1;
		  end
		  n=n+1+1;
	   end
	   if n>=N;
		  break;
	   end
	end
end
