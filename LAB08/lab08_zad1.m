imgBMP = imread('lennacolor24b.bmp');

excerpt=imgBMP(1:8,1:8,:);

%zamknac w funkcji getRGB
R=excerpt(:,:,1);
G=excerpt(:,:,2);
B=excerpt(:,:,3);
%%%%%%%%%%%%

%rgb2yuv
Y=0.3*R+0.56*G+0.11*B;
U=0.56*(B-Y)+128;
V=0.71*(R-Y)+128;
%%%%%%%%%%%%%

%mapvalue
mY=Y-128;
mU=U-128;
mV=V-128;
%%%%%%%%%%%%%

%dyskretna transformacja kosinusowa - FDCT (wej�ciowe to f, wyj�ciowe to F)
%wywo�a� funkcje dla 3 macierzy oddzielnie
F=zeros(8,8);
for u= 1:8
    for v=1:8
        temp=0;
        for x=1:8
            for y=1:8 
                temp=temp+(f(x,y)*cos (((2*(x-1)+1)*(u-1)*pi)/16)*cos(((2*(y-1)+1)*(v-1)*pi)/16));
                
                %dodatkowy pliczek C 
                
                %function [x]=C(x)
                %if x==0
                %    x=1/(sqrt(2));
                %else x=1;
                %end
                %end
                
                %koniec dodatkowego pliczku c
                
            end
        end
        F(u,v)=(1/4)*C(u-1)*C(v-1)*temp;
    end
end

%%%%%%%%%%%%%%%%%%

%kwantyzacja wraz z kompresj� uzale�nion� od parametru "c"
Qy = ...
   [ 16  11  10  11  24  40  51  61
     12  12  14  19  26  58  60  55
     14  13  16  24  40  57  69  56
     14  17  22  29  51  87  80  62
     18  22  37  56  68 109 103  77
     24  35  55  64  81 104 113  92
     49  64  78  87 103 121 120 101
     72  92  95  98 112 100 103  99];

%macierz kwantyzacji dla wsp�czynnik�w chrominancji
Quv = ...
   [ 17  18  24  47  99  99  99  99
     18  21  26  66  99  99  99  99
     24  26  56  99  99  99  99  99
     47  66  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99];
 
 Qy=Qy*c;
 Quv=Quv*c;
 
 qY=Round(fY/Qy);
 qU=Round(fU/Quv);
 qV=Round(fV/Quv);
 
 %%%%%%%%%%%%%%%%
 
 %reorganizacja macierzy na wektory
 idx=[1,9,2,3,10,17,25,18,11,4,5,12,19,26,33,41,34,27,20,13,6,7,14,21,38,35,42,49,57,50,43,36,29,22,15,8,16,23,30,37,44,51,58,59,52,45,38,31,24,32,39,46,53,60,61,54,47,40,48,55,62,63,56,64] %metoda zig-zag
 zY=qY(idx);
 zU=qU(idx);
 zV=qV(idx);
 
 %dla ujemnych -> dodatnie nieparzyste ((*-2)+1), dodatnie -> dodatnie*2
 
 %kodowanie RLE na repozytorium 
 
 %%%%%%%%%%%%%%%
 