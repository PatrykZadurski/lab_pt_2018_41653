function [FU] = YUV2FDCTb(fU)
FU=zeros(8,8);
for u= 1:8
    for v=1:8
        temp=0;
        for x=1:8
            for y=1:8 
                temp=temp+(fU(x,y)*cos(((2*(x-1)+1)*(u-1)*pi)/16)*cos(((2*(y-1)+1)*(v-1)*pi)/16));               
            end
        end
        FU(u,v)=(1/4)*C(u-1)*C(v-1)*temp;
    end
end
end