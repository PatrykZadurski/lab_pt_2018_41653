function[Y,U,V] = rgb2yuv(R,G,B)
Y=0.3*R+0.56*G+0.11*B;
U=0.56*(B-Y)+128;
V=0.71*(R-Y)+128;
end