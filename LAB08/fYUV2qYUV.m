function [ qY, qU, qV ] = fYUV2qYUV( fY, fU, fV, Qy, Quv, c )
 Qy=Qy*c;
 Quv=Quv*c;
 
 qY=round(fY./Qy);
 qU=round(fU./Quv);
 qV=round(fV./Quv);
end