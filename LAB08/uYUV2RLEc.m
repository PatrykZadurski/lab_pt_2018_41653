function [ rV ] = IRLE64( uV )

	N=length(uV);
	n=1;

	I=1;
	rV(I)=int8(0);

	while 1
	   if uV(n)<0
		  % unique reading
		  for k=n+1:n+abs(uV(n))
			 rV(I)=uV(k);
			 I=I+1;
		  end
		  n=n+abs(uV(n))+1;
	   else
		  % repeatable reading
		  for k=1:uV(n)
			 rV(I)=uV(n+1);
			 I=I+1;
		  end
		  n=n+1+1;
	   end
	   if n>=N;
		  break;
	   end
	end
end