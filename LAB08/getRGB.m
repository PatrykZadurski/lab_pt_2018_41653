function [R,G,B] = getRGB(excerpt)
R=excerpt(:,:,1);
G=excerpt(:,:,2);
B=excerpt(:,:,3);
end