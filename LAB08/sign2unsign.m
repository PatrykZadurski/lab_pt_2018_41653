function [ uY, uU, uV ] = sign2unsign( zY, zU, zV )

uY=zeros(8,8);
uU=zeros(8,8);
uV=zeros(8,8);

y=length(zY);

for a=1:y
if zY<0
uY=zY*(-2)+1;
end

if zY>0
uY=zY*2;
end


end



u=length(zU);

for a=1:y
if zU<0
uU=zU*(-2)+1;
end

if zU>0
uU=zU*2;
end

end


v=length(zV);

for a=1:v
if zV<0
uV=zV*(-2)+1;
end

if zV>0
uV=zV*2;
end

end

end