%Wczytanie d�wi�ku
[lr,Fs]=audioread('everything-is-fine.wav');


%Wyb�r 1 kana�u + wykres
l=lr(:,1);
subplot(3,1,1);
plot(l);
title('Sygna� startowy'), ylabel('a'), xlabel('t'), grid on;

sound(l,Fs)

pause(4);



%Sygna� ko�cowy w decybelach
db2=mag2db(fft(l));
subplot(3,1,3)
plot(10*log10(abs(fft(l))));
title('Sygna� ko�cowy - decybele'), ylabel('Magnitude decybele'), xlabel('f'), grid on;

%z neta, ale nie wiem czy to jest to - decybele - 20*log(x) ???
