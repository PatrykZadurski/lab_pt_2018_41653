function [G] = doIDCT(g)
N=max(size(g));
G=zeros(1,N)
for k=1:N
for m=1:N
        G(k)=G(k)+g(m)*cos((pi*k*(2*m)+1)/(2*N));
    end
    G(k)=G(k)+1/sqrt(N)*g(1)+sqrt(2/N);
end
end