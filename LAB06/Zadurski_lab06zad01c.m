% Badanie sygna�u 2-D
% Odczyt pliku graficznego
xy = imread('lennagrey.png');
% Przekszta�cenie formatu RGB na 2-D odcienie szaro�ci
xy = rgb2gray(xy);
% Rysowanie
figure,imshow(xy)
title('Obraz na poczatku');

% Obliczenie dwuwymiarowego fft
XY=fft2(xy);

figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar;
title('Obliczenie dwuwymiarowego fft');
xlabel('x');
ylabel('y');
zlabel('magnitude');


% Obr�t w symetrii transformaty
XY=fftshift(XY);

figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar;
title('Obrot w symetrii transformaty');
xlabel('x');
ylabel('y');
zlabel('magnitude');

% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.4;
% Zerowanie pewnego pasma dolnych cz�stotliwo�ci
XY(256-floor(128*q):256+floor(128*q),256-floor(128*q):256+floor(128*q))=0;

figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar;
title('Zerowanie pasma dolnych czestotliwosci');
xlabel('x');
ylabel('y');
zlabel('magnitude');

% Obr�t w symetrii transformaty do pierwotnej postaci
XY=fftshift(XY);

figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar
title('Obrot w symetrii transformaty do pierwotnej postaci');
xlabel('x');
ylabel('y');
zlabel('magnitude');

% Odwrotna transformata 2-D
xy=real(ifft2(XY));

figure,imshow(xy);
title('Obraz po przeksztalceniu');


I = imread('lennagrey.png');
Iblur = imgaussfilt(I, 2);
subplot(1,2,1)
imshow(I)
title('Original Image');
subplot(1,2,2)
imshow(Iblur)
title('Gaussian filtered image, \sigma = 2');
Iblur=uint8(fftshift(Iblur));

figure,mesh(10*log10(abs(Iblur))),colormap(gca,jet(64)),colorbar