function [G] = doDCT(g)
N=max(size(g));
G=zeros(1,N)
for m=1:N
    G(1)=G(1)+g(m);
end
G(1)=G(1)/sqrt(N);
for k=2:N
    for m=1:N
        G(k)=G(k)+g(m)*cos((pi*k*(2*m)+1)/(2*N));
    end
    G(k)=G(k)*sqrt(2/N);
end
end