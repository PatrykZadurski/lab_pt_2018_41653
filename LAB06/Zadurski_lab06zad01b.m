% Wczytanie pliku
[lr,Fs]=audioread('everything-is-fine.wav');

subplot(2,2,1);
plot(lr);
title('Sygna� pocz�tkowy'), ylabel('a'), xlabel('t'), grid on;

%Wyb�r tylko jednego kana�u
l=lr(:,1);

% Obliczenie Fs-punktowej transformaty
L=fft(l,Fs);
subplot(2,2,2);
plot(abs(L));
title('Pocz�tkowy FFT'), ylabel('a'), xlabel('f'), grid on;

% Odtworzenie sygna�u l, bez zerowania sk��dowych
sound(l,Fs)

% Odczekanie 4s
pause(4);

% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.9;

% Obracanie widma
L=fftshift(L);

% Obliczenie progu odci�cia i wyzerowanie kranc�w widma
r=floor((Fs/2)*q);
L(1:r)=0;
L(Fs-r:Fs)=0;

% Obracanie widma do pierwotnej kolejno�ci
L=fftshift(L);
subplot(2,2,3);
plot(abs(L));
title('Ko�cowy FFT'), ylabel('a'), xlabel('f'), grid on;

% Odwrotna transformata
l=real(ifft(L));

subplot(2,2,4);
plot(l);
title('Sygna� ko�cowy'), ylabel('a'), xlabel('t'), grid on;

% Odtworzenie sygna�u l z wyzerowan� cz�ci� widma
sound(l,Fs);

