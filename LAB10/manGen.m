function[M]=manGen(TTL,CLK)
M=zeros(length(TTL),1);
d=0;
for k=1:1:length(CLK)
    if((CLK(k+1))<>(CLK(k)))
        if(d==0) d=1; 
        end
        if(CLK(k+1)>(CLK(k)))
          if(TTL(k)==TTL(k+1))         
                  d=d*(-1);
          else
            if(TTL(k)==1 && d==(-1))
             d=1;
            else 
            d=-1;
            end
          end
        end
    end
M(k)=d;    
end