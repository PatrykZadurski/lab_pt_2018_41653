function [v] = v(CLK,data,sp_clk)
dlugosc=length(data);
Nv=dlugosc*sp_clk;
v=zeros(Nv,1);
n=1;
for k=1:sp_clk:Nv
    v(k:k+sp_clk-1)=data(n);
    n=n+1;
end
end