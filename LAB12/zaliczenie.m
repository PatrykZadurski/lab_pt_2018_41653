close all
clear all
clc

% Wiadomo�� do przes�ania 
 msg='patrykzadurski';

% Dopuszczalny s�ownik kodowy
 alphabet=['p','a','t','r','y','k','z','d','u','s','i'];

% Szacowanie prawdopodobie�stw wyst�pienia znak�w ze s�ownika kodowego
[ sortedAlphabet, sortedProbabilityDistribution ] = getProb( msg, alphabet );

% Rysowanie wykresu prawdopodobie�stwa wyst�pienia znak�w ze s�ownika kodowego
plotProb( sortedAlphabet, sortedProbabilityDistribution );

% Obliczanie drzewa kodowego Huffmana
[ codeBook ] = huffTree( sortedAlphabet, sortedProbabilityDistribution )

% Kodowanie danych na podstawie obliczonego drzewa
[ encodeMsg ] = encodeHuff( codeBook, msg );

data =[1 1 1 1]'
data2 = [0 1 1 1]'

encode_data=h74_encode(data)
encode_data2=h74_encode(data2)

daata=[encode_data encode_data2];

%Bod (ang. Baud), typowe warto�ci: 1200/2400/4800/9600
bod=10;

%% zmienne pomocnicze
sim_fs=bod*100; %cz�stotliwo�� pr�bkowania dla symulacji 100 wi�ksza od Bod
sp_clk=sim_fs/bod; %sampli na cykl zegara
N = length(daata); %ilo�� bit�w w danych wej�ciowych
sp_d=N*sp_clk; %sampli na ilo�� danych
t = 0:(1/sim_fs):((sp_d/sim_fs)-(1/sim_fs)); %czas trwania sygna�u
empty = zeros(1,length(t));%pomocniczy pusty wektor 

CLK     = clkGenerator(sp_clk,sp_d);%sygna� zegara

%% zadanie w�a�ciwe
%strumie� po kodowaniu
%gdy napiszesz w�asn� funkcj� koduj�c� zamie� "empty" na w�a�ciw� nazw� funkcji
TTL     = v(CLK,daata,sp_clk);
MAN     = empty;%(TTL,CLK);
NRZI    = empty;%nrziGenerator(CLK,data);
BAMI    = empty;%bamiGenerator(CLK,data);

%% pomocnicza funkcja rysuj�ca
plotLinearEncodeSignal(t, CLK, TTL, MAN, NRZI, BAMI);